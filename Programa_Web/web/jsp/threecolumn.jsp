<%-- 
    Document   : threecolumn
    Created on : 05-oct-2016, 20:02:33
    Author     : Juan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>CONTROL DE ACCESO</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>
        <!-- Codigo JS-->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="../js/skel.min.js" type="text/javascript"></script>
        <script src="../js/skel-panels.min.js" type="text/javascript"></script>
        <script src="../js/init.js" type="text/javascript"></script>
        <script src="../js/main.js" type="text/javascript"></script>
        <script src="../js/html5shiv.js" type="text/javascript"></script>
        <!--Codigo css3-->
        <link href="../css/style-desktop.css" rel="stylesheet" type="text/css"/>
        <link href="../css/skel-noscript.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <link href="../css/ie/v8.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style-1000px.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="header">
            <div class="container">
                <div id="logo">
                    <h1><a href="#">REGISTRO DE USUARIOS NUEVOS</a></h1>
                </div>
                <nav id="nav">
                    <ul>
                        <li><a href="index.jsp">PAGINA DE INICIO</a></li>
                        <li class="active"><a href="threecolumn.jsp">REGISTRAR USUARIOS</a></li>
                        <li><a href="twocolumn1.jsp">CONSULTAR</a></li>
                        <li><a href="twocolumn2.jsp">EDITAR USUARIOS</a></li>
                        <li><a href="onecolumn.jsp">GENERAR REPORTES</a></li>
                    </ul>
                </nav>

            </div>
        </div>
        <div id="main">
            <div class="container">
                <div class="row">
                    <div id="sidebar" class="3u">
                        <section>
                            <header>
                                <h2>Registro de fechas</h2>
                                <span class="byline">Praesent lacus congue rutrum</span>
                            </header>
                            <p>Modificar informacion</p>
                            <ul class="default"></ul>
                        </section>
                    </div>
                    <div id="content" class="6u skel-cell-important">
                        <section>
                            <header>
                                <h2>Datos de contacto</h2>
                                <span class="byline">Direcciones y numeros de contacto</span>
                            </header>
                            <p><a href="#" class="image full"><img src="images/pics02.jpg" alt=""></a></p>
                            <p>informacion modificar</p>
                        </section>
                    </div>
                    <div id="sidebar" class="3u">
                        <section>
                            <header>
                                <h2>Otros datos</h2>
                                <span class="byline">Datos de validacion</span>
                            </header>
                            <p>Informacion modificar</p>
                            <ul class="default">
                            </ul>
                        </section>
                    </div>
                </div>

            </div>
        </div>
        <div id="Uniminuto">
            <div class="container">
                <a>UNIMINUTO - PROGRAMACION WEB</a> 
            </div>
        </div>

    </body>
</html>

